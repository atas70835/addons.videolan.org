<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Addons\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

class DetailsController extends AbstractActionController
{
    protected $addonsTable;
    protected $addonsFileTable;

    public function updateNavLabels( $currentname )
    {
        foreach( array('details') as $id )
        {
            $page = $this->navigation->findBy('id', $id);
            if ( $page )
            {
                $page->setLabel( str_replace( '%1', $currentname, $page->getLabel() ) );
            }
        }
    }

    public function onDispatch(MvcEvent $e)
    {
        $this->navigation = $this->getServiceLocator()->get('navigation');

        if ( $this->params('uuid') )
        {       
            $page = $this->navigation->findBy('id', 'IndexRoot');

            $detailspage = new \Zend\Navigation\Page\Mvc(
                    array(
                        'id' => 'details',
                        'label' => '"%1"',
                        'route' => 'addondetails',
                        'params' => array('uuid' => $this->params('uuid') )
                    )
            );
            $detailspage->setRouter( $e->getRouter() );
            $detailspage->setRouteMatch( $e->getRouteMatch() );

            $page->addPage( $detailspage );
        }

        return parent::onDispatch($e);
    }

    public function indexAction()
    {
        $addon = $this->getAddonsTable()->getAddon( $this->params('uuid') );
        $this->updateNavLabels( $addon->name );
        return new ViewModel( array( 'addon' => $addon ) );
    }

    public function imageAction()
    {
        $addon = $this->getAddonsTable()->getAddon( $this->params('uuid') );

        $viewModel = new ViewModel();
        $viewModel->setTerminal( true );
        $this->getResponse()->setContent( $addon->imagedata );
        $this->getResponse()->getHeaders()->addHeaders( array(
            'Content-type' => 'image/png',
        ));

        return $this->getResponse();
    }

    private function addAuthorShip(&$addon)
    {
        if(!empty($addon->ref_userid))
        {
            $zfcmapper = $this->getServiceLocator()->get('zfcuser_user_mapper');
            $user = $zfcmapper->findById( $addon->ref_userid );
            $addon->email = $user->getEmail();
            $addon->creator = $user->getUsername();
        }
    }

    public function downloadAction()
    {
        $hydrator = new \Zend\Stdlib\Hydrator\ObjectProperty;

        $addon = $this->getAddonsTable()->getAddon( $this->params('uuid') );
        $addonfiles = $this->getAddonsFileTable()->fetch( $this->params('uuid') );

        $this->addAuthorShip($addon);

        foreach( $addonfiles as $addonfile )
            $addon->resources[] = $hydrator->extract($addonfile);

        $tmpname = tempnam('', $this->params('uuid'));
        $vlp = new \Addons\Model\Archive;

        $vlp->pack( $tmpname, $hydrator->extract( $addon ) );
     
        $viewModel = new ViewModel();
        $viewModel->setTerminal( true );
        //$this->getResponse()->setContent( file_get_contents( 'data/' . $addon->archive ) );
        
        $filename = preg_replace("/[^A-z0-9]/", '_', $addon->name . ' ' . $addon->version );

        $this->getResponse()->setContent( file_get_contents( $tmpname ) );
        $this->getResponse()->getHeaders()->addHeaders( array(
            'Content-type' => 'application/zip',
            'Content-disposition' => 'attachment; filename=' . $filename . ".vlp"
        ));

        unlink($tmpname);

        return $this->getResponse();
    }

    public function getAddonsTable()
    {
        if (!$this->addonsTable) {
            $sm = $this->getServiceLocator();
            $this->addonsTable = $sm->get('Addons\Model\AddonsTable');
        }
        return $this->addonsTable;
    }

    public function getAddonsFileTable()
    {
        if (!$this->addonsFileTable) {
            $sm = $this->getServiceLocator();
            $this->addonsFileTable = $sm->get('Addons\Model\AddonsFileTable');
        }
        return $this->addonsFileTable;
    }   
}
