<?php
namespace Addons\Model;

class AddonFile
{
    public $ref_uuid;
    public $type;
    public $filename;
    public $data;

    protected $fields = array('ref_uuid', 'type', 'filename', 'data');
    
    public function exchangeArray($data)
    {
        foreach( $this->fields as $v )
        {
            $this->$v = (isset($data[$v])) ? $data[$v] : null;
        }
    }

    static public function getValidator( $type )
    {
        switch( $type )
        {
            case 'ref_uuid':
                return new \Zend\Validator\Regex("/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/");
            case 'type':
                return new \Addons\Validator\AddonType;
            case 'filename':
                $chain = new \Zend\Validator\ValidatorChain();
                $chain->addByName( "StringLength", array('min'=>5, 'max'=>100) );
                $chain->addByName( "Regex", array( 'pattern' => "/^[A-z0-9]+[A-z0-9_\/\-\. ]+$/" ) );
                return $chain;
            default:
                return null;
        }
    }


    public function validateAll( )
    {
        foreach( $this->fields as $v )
        {
            $validator = self::getValidator($v);
            if ( $validator && !$validator->isValid($this->$v) )
            {
                $messages = $validator->getMessages();
                    if ( count($messages) )
                        throw new \Exception( $v . ": " . array_shift($messages) );
                    else
                        throw new \Exception( $v . ": is invalid" );
            }
        }

        return true;
    }
    
}
