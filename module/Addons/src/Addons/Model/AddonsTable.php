<?php

namespace Addons\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbTableGateway;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;

class AddonsTable
{
    protected $tableGateway;
    protected $lastInsertUUID;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function fetch( $type = null, $order = null, $like = null, $paginated = false, $status = null )
    {
        $params = array();
        if ( !empty( $type ) )
            $params['type'] = $type;

        if ( !empty( $status ) )
            $params['status'] = $status;

        switch( $order )
        {
            case 'name':
                $sortby = 'name ASC';
            break;
            case 'downloads':
                $sortby = 'downloads DESC';
            break;
            case 'rank':
            default:
                $sortby = 'score DESC';
        }

        if ( !empty( $like ) )
            $params[] = new \Zend\Db\Sql\Predicate\Like('name','%Tune%');

        if ( $paginated )
        {
            $dbTableGatewayAdapter = new DbTableGateway( $this->tableGateway, $params, $sortby );
            $paginator = new Paginator($dbTableGatewayAdapter);
            return $paginator;
        }

        $resultSet = $this->tableGateway->select( $params );
        return $resultSet;
    }

    public function updateStatus($uuid, $status)
    {
        $data = array(
            'status' => $status,
        );

        if ($this->getAddon($uuid))
            $this->tableGateway->update($data, array('uuid' => $uuid ));
        else
            throw new \Exception('invalid addon uuid');
    }

    public function getAddon($uuid)
    {
        $rowset = $this->tableGateway->select(array('uuid' => $uuid));
        $row = $rowset->current();
        return $row;
    }

    public function getAddonsByUser($id)
    {
        $resultSet = $this->tableGateway->select(array('ref_userid' => $id));
        return $resultSet;
    }

    protected function generateGuid()
    {
        if (function_exists('com_create_guid'))
        {
            return substr(com_create_guid(), 1, 36);
        } else
        {
            mt_srand((double) microtime() * 10000);
            $charid = strtolower(md5(uniqid(rand(), true)));

            $guid = substr($charid, 0, 8) . '-' .
                    substr($charid, 8, 4) . '-' .
                    substr($charid, 12, 4) . '-' .
                    substr($charid, 16, 4) . '-' .
                    substr($charid, 20, 12);
            return $guid;
        }
    }

    public function saveAddon(Addon $addon)
    {
        $data = array(
            'name' => $addon->name,
            'type' => $addon->type,
            'description' => $addon->description,
            'summary' => $addon->summary,
            'imagedata' => ''
        );

        $uuid = $addon->uuid;

        if ( empty($uuid) ) {
            if( empty($addon->ref_userid) )
                throw new \Exception('Trying to insert addon without owner');
            $data['ref_userid'] = $addon->ref_userid;
            $data['uuid'] = $this->generateGuid();
            $this->lastInsertUUID = null;
            $this->tableGateway->insert($data);
            $this->lastInsertUUID = $data['uuid'];
        } else {
            if ($this->getAddon($uuid)) {
                $data['imagedata'] =
                        new Expression( "DECODE('" .base64_encode( $addon->imagedata ) . "', 'base64')" );
                $this->tableGateway->update($data, array('uuid' => $uuid ));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function deleteAddon($uuid)
    {
        $this->tableGateway->delete(array('uuid' => $uuid));
    }

    public function getLastInsertValue()
    {
        return $this->lastInsertUUID;
    }
}