--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: user_role; Type: TABLE; Schema: public; Owner: addons; Tablespace: 
--

CREATE TABLE user_role (
    id integer NOT NULL,
    role_id character varying(255) NOT NULL,
    is_default smallint DEFAULT 0 NOT NULL,
    parent_id integer
);


ALTER TABLE public.user_role OWNER TO addons;

--
-- Name: user_role_id_seq; Type: SEQUENCE; Schema: public; Owner: addons
--

CREATE SEQUENCE user_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_role_id_seq OWNER TO addons;

--
-- Name: user_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: addons
--

ALTER SEQUENCE user_role_id_seq OWNED BY user_role.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: addons
--

ALTER TABLE ONLY user_role ALTER COLUMN id SET DEFAULT nextval('user_role_id_seq'::regclass);


--
-- Data for Name: user_role; Type: TABLE DATA; Schema: public; Owner: addons
--

INSERT INTO user_role VALUES (1, 'registered', 0, NULL);
INSERT INTO user_role VALUES (2, 'admin', 0, 1);
INSERT INTO user_role VALUES (3, 'guest', 1, NULL);


--
-- Name: user_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: addons
--

SELECT pg_catalog.setval('user_role_id_seq', 3, true);


--
-- Name: user_role_id_key; Type: CONSTRAINT; Schema: public; Owner: addons; Tablespace: 
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_role_id_key UNIQUE (id);


--
-- Name: idx_parent_id; Type: INDEX; Schema: public; Owner: addons; Tablespace: 
--

CREATE INDEX idx_parent_id ON user_role USING btree (parent_id);


--
-- Name: unique_role; Type: INDEX; Schema: public; Owner: addons; Tablespace: 
--

CREATE UNIQUE INDEX unique_role ON user_role USING btree (role_id);


--
-- Name: fk_parent_id; Type: FK CONSTRAINT; Schema: public; Owner: addons
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT fk_parent_id FOREIGN KEY (parent_id) REFERENCES user_role(id) ON DELETE SET NULL;


--
-- PostgreSQL database dump complete
--

